// Módulos requeridos
const express = require('express');
const http = require('http');
const helmet = require('helmet');  // seguridad
var compression = require('compression'); // compression del resultado
require('dotenv').config();
const {v4: uuidv4} = require('uuid');

const app = express();
app.use(helmet()); // Ayuda a proteger aplicaciones Express (seguridad)
app.use(compression()); //comprime el resultado, respuesta cliente mas rapida

// Servidor HTTP
const serverHttp = http.createServer(app);
serverHttp.listen(process.env.HTTP_PORT, process.env.IP); // configuracion .env
//console.log(process.env.HTTP_PORT)

// Contenido estático
app.use(express.static('./public'));

// Prueba API
app.get('/api/get-uuid', function(req, res) {
    //res.send("Hola mundo")
    res.send(uuidv4());
});

// 404
app.get('*', function(req, res) {
    res.status(404).send('Error 404 - Recurso no encontrado')
});

